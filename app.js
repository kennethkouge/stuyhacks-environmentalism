const express = require('express');
const fs = require("fs")
const path = require('path')
const bodyParser = require('body-parser');
const helmet = require('helmet')
const csrf = require('csrf')

const multer = require('multer')

const router = require('./router')

const app = express();

const middleware = [
    express.static('public'),
    bodyParser.json(),
//    bodyParser.urlencoded({extended: true}),
    helmet()
]

app.use(middleware) 
express.static('public')
app.use('/uploaded_images', express.static('uploaded_images'))
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')
app.use('/', router)

app.get('/', function(req, res) {
    res.send("Hello, world!")
})

app.use((req, res, next) => {
    res.status(404).render("error", {
        code: "404",
        reason: "Page Not Found",
        description: "The page you are looking for does not exist."
    })
})

const port = 8080

app.listen(8080, () => console.log(`App listening on port ${port}`));