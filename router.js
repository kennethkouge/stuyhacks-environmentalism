const express = require('express');
const path = require('path')
var fs = require('fs');
var uuid = require('uuid')
var sqlite3 = require('sqlite3').verbose();
const multer = require('multer')

const router = express.Router();

var file = path.join(__dirname, '/database.db')
var exists = fs.existsSync(file);

const imageTableName = "images"

if (!exists) {
    console.log('Creating DB file');
    fs.openSync(file, 'w');
}

var db = new sqlite3.Database(file);

if (!exists) {
    db.run(`CREATE TABLE ${imageTableName} (id integer primary key, dirty TEXT, clean TEXT, hours_worked integer, location TEXT, timestamp INTEGER)`)
}

function loadImages(callback){
    db.all(`SELECT * FROM ${imageTableName} ORDER BY timestamp DESC`, function (e, r) {
        if(e)
            throw e;

        callback(r)
    })
}

const nav = [
    {
        link: '/',
        label: 'Homepage'
    },
    {
        link: '/report',
        label: 'Report'
    },
    {
        link: '/about',
        label: 'About'
    }
]

router.get('/', function (req, res) {
    var images = loadImages((x) => {
        res.status(200).render('index2.ejs', {
            entries: x,
            nav: nav
        });
    })
})

router.get('/report', function(req, res) {
    res.status(200).render('report.ejs', {
        nav: nav
    })
})

router.get('/about', function(req, res){
    res.status(200).render('about.ejs', {
        nav: nav
    })
})

router.get('/dirtyloc/:id', function(req, res) {
    const id = req.params.id;
    
    db.get(`SELECT * FROM ${imageTableName} WHERE id = ?`, id, (e, r) => {
        if(e)
            throw e;
        
        res.status(200).render('fix.ejs', {
            entry: r
        })
    })
})

const images = path.join(__dirname, "uploaded_images")
const notYetCleaned = "notyetcleaned.png"

const upload = multer({
    dest: images,
    limits: { fileSize: 5000000 },
    fileFilter: function (req, file, cb) {
        if (!isImage(path.extname(file.originalname))) {
            return cb(new Error('Only images are allowed'))
        }

        cb(null, new Date().getTime() + '.png')
    }
});

router.post('/clean/:id', upload.single("file"), function(req, res) {
    const tempPath = req.file.path
    const ext = path.extname(req.file.originalname)

    console.log("extension");
    console.log(ext)

    const newName = new Date().getTime() + ext;
    
    console.log(tempPath)

    fs.rename(tempPath, path.join(images, newName), err => {
        if(err)
            throw err;

        console.log(req.body.hours)

        //bring recently cleaned posts to the top even if the trash was reported a long time ago
        db.run(`UPDATE '${imageTableName}' SET clean = '${newName}', timestamp = '${new Date().getTime()}', hours_worked = '${req.body.hours}' where id = ${req.params.id}`, (e) => {
            if(e)
                throw e;
            res.redirect('/')
        })
    })
})

function isImage(x) {
    if (x == '.png')
        return true;
    if (x == '.jpeg' || x == '.jpg')
        return true;
    if(x == '.webp')
        return true;
    return false;
}

router.post('/upload', upload.single("file"), (req, res) => {    
    const tempPath = req.file.path
    const ext = path.extname(req.file.originalname)

    console.log("extension");
    console.log(ext)

    const newName = new Date().getTime() + '.png';
    
    console.log(tempPath)

    fs.rename(tempPath, path.join(images, newName), err => {
        if(err)
            throw err;

        db.run(`INSERT INTO '${imageTableName}' (dirty, clean, location, timestamp) VALUES (?, ?, ?, ?)`, [newName, notYetCleaned, req.body.location, new Date().getTime()])

        res.redirect('/')
    })
})

module.exports = router;